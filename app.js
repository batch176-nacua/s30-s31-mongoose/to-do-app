//Include Installing mongoose in the terminal
//npm install express nodemon mongoose
//NOTE: git init first before making a .gitignore to ignore node_modules

// Dependencies and Modules
	//Check first if "express" & "mongoose" are installed in your dependencies
	//To check - package.json -> dependencies
	const express = require("express");
	const mongoose = require("mongoose");

// Server Setup (Establish a connection & Define a path/address)
	const app = express();
	//Assign the port 4000 instead of 3000 to avoid cconflict in the future with frontend application that run on port 3000
	const port = 4000; 

// Database Connection
	//Connect to MongoDB Atlas
	/* Get the credentials of your mongoDB Atlas user
		1. Click Connect besides the Cluster
		2. Choose Connect your Application
		3. Update Driver -> node.js, Update Version -> 3.6 or later
		4. Copy the Connection String
		5. Change the username:<password> to your password
		6. Change Database name to your prefer location -> mongodb.net/<DBname>?retry...
			-> Good Note: If no DBName found, mongoDB will automitcally create it for you
	*/
	/* If you forgot password to your mongoDB cluster
		1. Click DB Access to the left navigation bar
		2. Click Edit Button
		3. Create a password/Auto generate secure password
		4. Update User first then input your password to <password>
	*/
	mongoose.connect('mongodb+srv://jaypraaa:vNBaDiPwm2E6GwEJ@cluster0.zjg95.mongodb.net/toDo176?retryWrites=true&w=majority',{

		//options to add to avoid deprecation warnings because of mongoose/mongodb update
		useNewUrlParser: true,
		useUnifiedTopology: true

	});

	//Create notifications if the connection to db is success or failure
		let db = mongoose.connection;
		//Add on() method from our mongoose connection to show the if success or failure in the terminal or browser
		db.on('error', console.error.bind(console, "Connection Error"));
		db.once('open', () => console.log("Connected to MongoDB"));

//Middleware -- A middleware, in expressJS context, are methods, functions that acts and adds features to our application
	app.use(express.json());

//Schema -> Like a template for your document's database. To avoid future error typos
	const taskSchema = new mongoose.Schema({
		// Define the fields for our documents and determine the appropriate data type
		name: String,
		status: String	

	})
//Mongoose Model
	/*
		Models are used to connect your apo to the corresponding collection in your database. It is a representation of your collection.

		 Models uses schemas to create object that correspond to the schema. 
		 NOTE: By default, when creating the collection from your model, the collection name is pluralized

		 Syntax: mongoose.model(<nameOfcollectionInAtlas>, <schemaToFollow>)
	*/

	//Model Variable should be capitalized first
	const Task = mongoose.model("task", taskSchema)
	//"task" will be pluralized in the Atlas as it is default

//Post route to create a new task
	app.post('/tasks', (req, res) => {
		//NOTE: Good practice -> When creating a new post/put to any route that requires data from client. First console log your req.body to check if the data transmitted is correct
			//console.log(req.body);
		//Creating a new task document by using the constructor of our Task model. This constructor should follow the schema of the model.
		let newTask = new Task({
			name: req.body.name,
			status: req.body.status

		})
	//.save() method will allow us to savve our document by connecting to our collection via our model
		/* 
		It has 2 approaches:
		1. Add anonymous function to handle the created document or error
			newTask.save((error, savedTask) => {
				if(error){
					res.send(error);
				}else {
					res.send(savedTask)
				}

			})
		2. We can add .then() which is used to handle proper result/returned value of a function and .catch() chain which will allow us to handle errors and created documents in seperate functions
		*/
			newTask.save()
			.catch(error => res.send({message: "Error in Document Creation"}))
			.then(result => res.send({message: "Document Creation Succesful"}))
			// .then(result => res.send(result))
			// .catch(error => res.send(error))
			
		
	})

	//Practice
		const sampleSchema = new mongoose.Schema({
			name: String,
			isActive: Boolean
		})


		//Model Variable should be capitalized first
		const Sample = mongoose.model("samples", sampleSchema);
		app.post('/samples', (req, res) => {
			let newSample = new Sample({
				name: req.body.name,
				isActive: req.body.isActive

			})

			newSample.save((error, savedSample) => {
				if (error) {
					res.send(error);
				} else {
					res.send(savedSample);
				}
			})
		})

	//Just another practice to create (STEPS)
		// //1. Schema
		// 	const manggagamitSchema = new mongoose.Schema({
		// 		username: String,
		// 		isAdmin: Boolean

		// 	})
		// //2. Model
		// 	const Manggagamit = mongoose.model("manggagamit", manggagamitSchema);
		// 	app.post("/mgaManggagamit", (req, res) => {
		// 		let newManggagamit = new Manggagamit({
		// 			username: req.body.username,
		// 			isAdmin: req.body.isAdmin
		// 		})

		// 		newManggagamit.save((savedManggagamit, error) => {
		// 			if (error) {
		// 				res.send(error)
		// 			} else {
		// 				res.send(savedManggagamit)
		// 			}
		// 		})
		// 	})

//GET Method Request
	app.get('/tasks', (req, res) => {
		//To query using mongoose, access the model of the collection first
		//Model.find() in mongoose is similar in function to mongoDb's db.collectionfind({})
		//Empty {} means empty criteria, means GET/RETRIEVE all
		Task.find({})
		.then(result => res.send(result))
		.catch(err => res.send(err));
	})

	app.get('/samples', (req, res) => {
		Sample.find({})
		.then(result => res.send(result))
		.catch(err => res.send(err))
	})


// ACTIVITY
	const userSchema = new mongoose.Schema({
		username: String,
		password: String
	})

	const User = mongoose.model("user", userSchema);
	//POST
	app.post('/users', (req, res) => {
		let newUser = new User({
			username: req.body.name,
			password: req.body.password

		})
		
		newUser.save()
		.catch(error => res.send({message: "Error in Document Creation"}))
		.then(result => res.send("User Added"))
	})

	//GET
	app.get('/users', (req, res) => {
		User.find({})
		.then(result => res.send(result))
		.catch(err => res.send(err))
	})
	


// Entry Point response
	//Bind the connection to the designated port
	app.listen(port, () => console.log(`Server running at port: ${port}`));