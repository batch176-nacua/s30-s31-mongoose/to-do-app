//Dependencies and Modules
	const express = require("express");
	const mongoose = require("mongoose");
	const taskRoute = require("./routes/taskRoute");

//Server Setup
	const app = express();
	const port = 4000;

//DB Connection
	mongoose.connect('mongodb+srv://jaypraaa:vNBaDiPwm2E6GwEJ@cluster0.zjg95.mongodb.net/toDo176?retryWrites=true&w=majority',{
		useNewUrlParser: true,
		useUnifiedTopology: true

	});

	let db = mongoose.connection;
	db.on('error', console.error.bind(console, "Connection Error"));
	db.once('open', () => console.log("Connected to MongoDB"));

//Middleware
	app.use(express.json());
	//For Front-End (I dont have any idea what this do)
	//app.use(express.urlencoded({extended: true}));

//Add the task route
	app.use("/tasks", taskRoute);

//Entry Point Response
	app.listen(port, () => console.log(`Server running at port: ${port}`));