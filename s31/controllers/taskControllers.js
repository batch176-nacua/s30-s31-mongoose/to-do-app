//Controllers contains the functions and the logics of our Express JS App

const Task = require("../models/task.js");

//Controllers

// Create
	module.exports.createTask = (requestBody) => {
		let newTask = new Task ({
			name: requestBody.name
		})

		return newTask.save().then((task, error) =>{
			if (error) {
				return false
			} else {
				return task
			}
		})
		//This won't work, idk why
			// return newTask.save()
			// .catch(error => return false)
			// .then(result => return result)
	}
// Retrieve
	module.exports.getAllTasks = () => {

		return Task.find({}).then(result => {
			return result
		})
	}
// Update
	//Updating Status from Pending to Complete
	module.exports.updateTaskToComplete = (taskId) => {
		return Task.findById(taskId).then((found, failed) => {
			if(found) {
				//To show the found output in the terminal
				console.log(found)
				origStatus = found.status
				found.status = 'Completed'
				return found.save().then((success, error) => {
					if(success) {
						return `${found.name}'s status has been UPDATED from ${origStatus} to ${found.status}`
					} else {
						return error
					}
				})
			} else {
				return 'Match not found';
			}
		})	
	}

	//Updating Status from Complete to Pending
	module.exports.updateTaskToPending = (userInput) => {
		return Task.findById(userInput).then((found, failed) => {
			if(found) {
				//To show the found output in the terminal
				console.log(found)
				origStatus = found.status
				found.status = 'Pending'
				return found.save().then((success, error) => {
					if(success) {
						return `${found.name}'s status has been UPDATED from ${origStatus} to ${found.status}`
					} else {
						return error
					}
				})
			}else {
				return 'Match not found'
			}
		})
	}



// Delete
	//1. Remove an existing resource inside TASK collection
	//taskId = variable reference of what to delete
	module.exports.deleteTask = (taskId) => {
		//Select which mongoose method will be used in order to acquire the desired goal
		//findByIdAndRemove() is a mongoose method. We still need to able to handle the outcome of the promise whatever state it may fall on
		/*Promises in JS
			=> Pending	=> Fulfilled/Success  => Rejected 
		*/
		return Task.findByIdAndRemove(taskId).then((success, failed) => {
			if(success) {
				return 'The Task has been successfully REMOVED';
			} else {
				return 'FAILED to remove Task';
			}
		})
	};




