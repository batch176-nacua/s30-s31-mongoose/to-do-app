//Create the Schema, Models
const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
})

//Put module.exports at the bottom of your js file so that it can be used in the app.js
module.exports = mongoose.model("Task", taskSchema);