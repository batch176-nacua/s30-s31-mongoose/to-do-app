//Contains all the endpoints for our application
//Since we seperate the routes from our server which is "app.js"
const express = require("express")

//Create a Router instance that functions as a middleware and routing system
//Allow access to HTTP method middlewares that makes it easier to create routes
const router = express.Router();

const taskController = require("../controllers/taskControllers.js")

//Routes
//Route for getting all the task
	router.get("/", (req, res) => {
		taskController.getAllTasks().then(resultFromController => 
			res.send(resultFromController))
	})

//Route for creating a task
	router.post("/", (req, res) => {
		taskController.createTask(req.body).then(resultFromController => 
			res.send(resultFromController))
	})

//Route for deleting a task
	router.delete('/:task', (req, res) => {
		//res.send('Correct route for DELETE')
		//Determine wether the value inside the path variable is transmitted to the server
			//params => Is in the Postman and 
			//task => is the name of the key variable inside Params
		console.log(req.params.task)

		let taskId = req.params.task
		//Path variable -> this will allow us to insert data within the scope of the URL
			/*How to use Path variable
				=> Add ":" at the start of the path to show the Path Variable
					Ex. /task then add ":" to make it /:task 
			*/
			//Why use path variable useful?
				//When inserting only a single piece of information/data
				//Also useful when passing down information to REST API method that does NOT include a BODY section like 'GET'
			//When integrating a path variable, you are also changing the behavior of the path from STATIC route to DYNAMIC
		taskController.deleteTask(taskId).then(resultOfDelete => res.send(resultOfDelete));
	})

//Route for Updating Task Status
	//Updating Status from Pending to Complete

	//Create a dynamic endpoint for this route
	router.put('/:task', (req, res) => {
		//Check if you are able to acquire the values inside the path variables
		console.log(req.params.task)

		let taskId = req.params.task
		taskController.updateTaskToComplete(taskId).then(resultOfUpdate => res.send(resultOfUpdate));
	})

	//Updating Status from Complete to Pending
	router.put('/:task/pending', (req, res) => {
		let taskId = req.params.task;

		//console.log(taskId) // => Test the initial setup

		taskController.updateTaskToPending(taskId).then(resultOfUpdate => res.send(resultOfUpdate))
	})


//Put module.exports at the bottom of your js file so that it can be used in the app.js
module.exports = router;